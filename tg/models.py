from django.db import models


class Polygon(models.Model): # Оснавная таблица с полигонами
    name = models.CharField(max_length=30, verbose_name='Имя полигона')

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = 'Полигон'
        verbose_name_plural = 'Полигоны'


class PolygonPoint(models.Model): # Таблица содержащая точки полигона
    id_polygon = models.ForeignKey(Polygon, on_delete=models.CASCADE, blank=False, null=False, default=None, verbose_name='Полигон')
    Num_Point = models.IntegerField(default=0, verbose_name='Порядковый номер точки') # Поле используется для сортировки и правильной отрисовки полигона
    Cord_x = models.IntegerField(default=0, verbose_name='Координата X')
    Cord_y = models.IntegerField(default=0, verbose_name='Координата Y')

    class Meta:
        verbose_name = 'Точка полигона'
        verbose_name_plural = 'Точки полигона'
