import json
from django.http import JsonResponse
from django.shortcuts import render
from tg.models import PolygonPoint,Polygon


def index(request):

    return render(request, 'index.html')

# Определение принадлежности точки к полигону


def check_polygon(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        in_polygon = False
        x = data['point'][0]
        y = data['point'][1]
        # Принадлежность точки к полигону проверяем методом Трассировки Луча

        for i in range(len(data['polygon'])):
            xp = data['polygon'][i][0]
            yp = data['polygon'][i][1]
            xp_pr = data['polygon'][i - 1][0]
            yp_pr = data['polygon'][i - 1][1]
            if (((yp <= y and y < yp_pr) or (yp_pr <= y and y < yp)) and (
                    x > (xp_pr - xp) * (y - yp) / (yp_pr - yp) + xp)):
                in_polygon = not in_polygon

        response = {u'in_polygon': in_polygon, u'point': [x, y]}
        return JsonResponse(response)


def get_polygon(request):

    polygon = []
    # Данные в БД заносятся через админку логин/пароль admin/admin
    # Здесь передаем напрямую что id_polygon = 1
    obj_polygon = PolygonPoint.objects.filter(id_polygon=1).order_by('Num_Point')

    for pointObj in obj_polygon:
        polygon.append([pointObj.Cord_x, pointObj.Cord_y])

    response = {u'polygon': polygon}
    return JsonResponse(response)

