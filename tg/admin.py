from django.contrib import admin
from tg.models import Polygon,PolygonPoint

admin.site.register(Polygon)
admin.site.register(PolygonPoint)
