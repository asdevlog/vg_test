$(function() {



    var polygon = [];
    var canvasWidth = 800;
    var canvasHeight = 500;

    // Функция отрисовки полигона из js файла
    var drawPol= function (id,coordinates) {
        var canvas=document.getElementById(id);
        var ctx=canvas.getContext("2d");
        ctx.canvas.width = canvasWidth;
        ctx.canvas.height = canvasHeight;
        ctx.beginPath();
        ctx.moveTo(coordinates[0][0], coordinates[0][1]);
        for (var i = 0; i < coordinates.length; i++){
            ctx.lineTo(coordinates[i][0], coordinates[i][1]);
        }
        ctx.strokeStyle = "red";
        ctx.stroke();

    };

    //Отлавливаем нажание на canvas
    $(document).on("click", "#canvas-main", function(event){
        // Получаем  координаты клика
        var x = event.pageX-$(this).offset().left;
        var y = event.pageY-$(this).offset().top;
        var canvas=document.getElementById("canvas-main");
        var ctx=canvas.getContext("2d");

        // в связи с тем что без csrftoken post запросы не работают необходимо вытащить csrftoken из Cookie
        function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

        //Добавляем Cookie в Ajax запрос
        $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
        });
        var query = {
            "polygon": polygon,
            "point": [x, y]
        };
        $.ajax({
            type: "POST",
            url: 'check_polygon/',
            data: JSON.stringify(query),
            success: function(data) {
                var res=document.getElementById("check-result");
                var checkDraw=document.getElementById("draw-point-status");
                //Обрабатываем результат запроса
                if (data['in_polygon'])
                    res.textContent ="Результат: Точка входит в полигон" ;
                else
                    res.textContent="Результат: Точка не входит в полигон";
                //Если выбран chekbox "Отрисовывать точки после нажатия " , отрисовываем точку
                if (checkDraw.checked)
                {   p = data['point'];
                // Рисуем круг в точке клика
                ctx.beginPath();
                ctx.arc(p[0], p[1],2, 0, 2 * Math.PI, false);

                // По результату запроса заливаем круг зеленым или красным цветом
                if (data['in_polygon'])
                    ctx.fillStyle = "#73AD23";
                else
                    ctx.fillStyle = "#FF0101";

                ctx.fill();}
            }
        });

    });

    // Функция получения координат полигона с сервера
    var drawPolDjango = function(id){
        $.ajax({
            type: "GET",
            url: 'get_polygon/',
            success: function(data) {
                polygon=data["polygon"];
                drawPol(id,polygon);
            }
        })
    };

    drawPolDjango("canvas-main")

});